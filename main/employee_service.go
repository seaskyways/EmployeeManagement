package main

import (
	"EmployeeManagement/employee"
)

func main() {
	employee.MigrateDatabase()
	employee.SetupAPI()
}
