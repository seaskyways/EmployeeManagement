package employee

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	db = GetDB()
)

func SetupAPI() {
	defer db.Close()
	router := gin.Default()

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"success": false})
	})

	router.GET("/employees", getEmployees)
	router.POST("/employees", saveEmployee)

	setupKidsRoutes(router)

	router.Run(":8080")
}

func getEmployees(c *gin.Context) {
	var emps []Employee
	db.Find(&emps)
	c.JSON(http.StatusOK, emps)
}

//noinspection GoStructTag
type employeeForm struct {
	id        uint   `json:"id"`
	firstName string `json:"first_name" binding:"required"`
	lastName  string `json:"last_name" binding:"required"`
	bDate     uint   `json:"b_date" binding:"required"`
}

func saveEmployee(c *gin.Context) {
	var form employeeForm
	if err := c.ShouldBindJSON(&form); err == nil {
		employee := &Employee{
			Model: gorm.Model{
				ID: form.id,
			},
			FirstName: form.firstName,
			LastName:  form.lastName,
			BDate:     time.Unix(int64(form.bDate), 0),
		}
		db.Save(employee)
		c.JSON(http.StatusOK, employee)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"success": false})
	}
}
