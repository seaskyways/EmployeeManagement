package employee

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func setupKidsRoutes(r *gin.Engine) {
	g := r.Group("/kids")
	g.GET("/", func(c *gin.Context) {
		var kids []Kid
		db.Find(&kids)
		c.JSON(http.StatusOK, kids)
	})

	g.GET("/:parentID", func(c *gin.Context) {
		var kids []Kid
		parentID, err := strconv.Atoi(c.Param("parentID"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false})
			return
		}

		db.Where(&Kid{ParentID: parentID}).
			Find(&kids)

		c.JSON(http.StatusOK, kids)
	})
}
