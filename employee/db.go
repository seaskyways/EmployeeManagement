package employee

import "github.com/jinzhu/gorm"
import (
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"time"
)

type Employee struct {
	gorm.Model
	FirstName, LastName string
	BDate               time.Time
	Kids []Kid
}

type Kid struct {
	gorm.Model
	ParentID int
	Parent Employee
	Name   string
}

func GetDB() *gorm.DB {
	db, err := gorm.Open("sqlite3", "emp.sqlite")
	if err != nil {
		panic(err)
	}
	return db
}

func MigrateDatabase() {
	db := GetDB()
	defer db.Close()

	db.Exec("PRAGMA journal_mode = WAL")

	db.AutoMigrate(
		&Employee{},
		&Kid{},
	)
}
